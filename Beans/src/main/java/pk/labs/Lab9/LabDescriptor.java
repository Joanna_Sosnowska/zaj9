package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.Fabryka;
import pk.labs.Lab9.beans.impl.Konsultacja;
import pk.labs.Lab9.beans.impl.ListaKonsultacji;
import pk.labs.Lab9.beans.impl.Termin;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = Termin.class;
    public static Class<? extends Consultation> consultationBean = Konsultacja.class;
    public static Class<? extends ConsultationList> consultationListBean = ListaKonsultacji.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = Fabryka.class;
    
}
