/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.*;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author Joanna667
 */
@XmlRootElement
public class Konsultacja implements Consultation,Serializable{
    private String student;
    private Term term;
    private final VetoableChangeSupport vcs = new VetoableChangeSupport(this);
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public Konsultacja(){
        
    }
    @Override
    public String getStudent() {
        return student;
    }

    @Override
    public void setStudent(String student) {
       this.student=student;
    }

    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
       return this.term.getEnd();
    }

    @Override
    @XmlElement
    public void setTerm(Term term) throws PropertyVetoException { Term staryTermin = this.term;
    vcs.fireVetoableChange("Term", staryTermin, term);
    this.term = term;
    pcs.firePropertyChange("Term", staryTermin, term);
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException { 
    if(minutes <= 0) minutes = 0;
    int staryPrzedzial = this.term.getDuration();
    this.vcs.fireVetoableChange("Term", staryPrzedzial, staryPrzedzial + minutes);
    this.term.setDuration(this.term.getDuration()+minutes);
    this.pcs.firePropertyChange("Term", staryPrzedzial, staryPrzedzial + minutes);
    }
    
}
