/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import java.util.List;
import pk.labs.Lab9.beans.*;

/**
 *
 * @author Joanna667
 */
public class ListaKonsultacji implements ConsultationList, PropertyChangeListener{
    private Consultation[] consultations;
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public ListaKonsultacji(){
    this.consultations = new Consultation[]{};
    }
    @Override
    public int getSize() {return this.consultations.length;}

    @Override
    public Consultation[] getConsultation() { return this.consultations;  }

    @Override
    public Consultation getConsultation(int index) {return this.consultations[index];}

    @Override
    public void addConsultation(Consultation c) throws PropertyVetoException {
        for (Consultation consultation1 : this.consultations) {
            if ((c.getEndDate().after(consultation1.getBeginDate()) && c.getBeginDate().before(consultation1.getBeginDate())) || (c.getBeginDate().before(consultation1.getEndDate()) && c.getEndDate().after(consultation1.getBeginDate()))) {
                throw new PropertyVetoException("Error", null);
            }

        } 
        Consultation[] nowa = Arrays.copyOf(this.consultations, this.consultations.length + 1); 
        Consultation[] stara = this.consultations; 
        nowa[nowa.length - 1] = c; this.consultations = nowa; 
        pcs.firePropertyChange("consultation", stara, nowa);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
       this.pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
       
    }
    
}
