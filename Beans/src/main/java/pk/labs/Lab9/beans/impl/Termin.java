/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.util.Date;
import javax.xml.bind.annotation.*;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author Joanna667
 */
@XmlRootElement
public class Termin implements Term{
    private Date begin;
    private int duration;
    public Termin(){
        
    }
    @Override
    public Date getBegin() {
        return begin;
    }

    @Override
    @XmlElement
    public void setBegin(Date begin) {
       this.begin=begin;
    }

    @Override
    public int getDuration() {
       return duration;
    }

    @Override
    @XmlElement
    public void setDuration(int duration) {
        if(duration>0)
           this.duration=duration;
        else
           this.duration=30;
    }

    @Override
    public Date getEnd() {
        return new Date(this.begin.getTime() + this.duration*60000);
    }
    
}
