/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

/**
 *
 * @author Joanna667
 */
public class Fabryka implements ConsultationListFactory, Serializable{
      @Override
    public ConsultationList create() {
              
        return new ListaKonsultacji();
    }

    @Override
    public ConsultationList create(boolean deserialize)
    {
        ConsultationList lista = new ListaKonsultacji();
        try {
            XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new  FileInputStream("plik.xml")));
            
             lista = (ListaKonsultacji)decoder.readObject();
            decoder.close();
           
      
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Fabryka.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista; 
    }

    @Override
    public void save(ConsultationList consultationList
    ) {
        try {
            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("plik.xml")));
            encoder.writeObject(consultationList);
            encoder.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Fabryka.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
